# Property Model
class Property < ActiveRecord::Base
  include ElasticSearchable
  include ActivityHistory
  has_many :images, dependent: :destroy
  has_many :questions, dependent: :destroy
  enum property_type: [:casa, :apartamento, :terreno, :edificio, :local]
  enum operation_type: [:venta, :alquiler]
  accepts_nested_attributes_for :images,
                                reject_if: :all_blank,
                                allow_destroy: true

  # Validations
  validates_presence_of :property_type, :code, :city, :zone, :size,
                        :reference, :operation_type, :price
  validates_uniqueness_of :code
  validates :images, presence: true, on: [:update, :create]
  validates :images, length: { maximum: 4 }
  validates :price, numericality: { greater_than: 0 }
  validates :price, numericality: { greater_than: 0 }
  validates :size, numericality: { greater_than: 0 }
  validates :rooms, numericality: { greater_than_or_equal_to: 0 }, allow_blank: true
  validates :bathrooms, numericality: { greater_than_or_equal_to: 0 }, allow_blank: true

  validate :images_present?, on: [:update, :create]

  def self.reload_index!
    __elasticsearch__.delete_index!
    __elasticsearch__.create_index!
    __elasticsearch__.import
  end

  settings analysis: {
    analyzer: {
      spanish_snowball: {
        type: 'snowball',
        language: 'Spanish',
        filter: %w(asciifolding lowercase)
      },
      autocomplete: {
        type:      'custom',
        tokenizer: 'standard',
        filter: %w(lowercase autocomplete_filter)
      }
    },
    filter: {
      autocomplete_filter: {
        type: 'edge_ngram',
        min_gram: 1,
        max_gram: 20
      }
    }
  } do
    mapping do
      [:id, :property_type, :code, :city, :zone,
       :reference, :operation_type, :price, :rooms,
       :bathrooms, :size, :description].each do |attribute|
        indexes attribute, type: 'string', analyzer: 'spanish_snowball'
      end
    end
  end

  def self.query(query)
    { query: { multi_match: {
      query: query,
      fields: [:property_type, :code, :city, :zone, :reference, :operation_type,
               :price, :rooms, :bathrooms, :size, :description],
      operator: :and,
      lenient: true }
    }, sort: { id: 'desc' }, size: count }
  end


  def self.app_searching(query)
    if query
      search(app_query(query)).records.order(id: :desc)
    else
      order(id: :desc)
    end
  end

  def self.app_query(query)
    {
      query: {
        bool: {
          should: [{
            match:  {
              _all: {
                query: query,
                fuzziness: 'auto'
              }
            }
          }]
        }
      }, size: count
    }
  end

  # Get the page number that the object belongs to
  def page(order = :id)
    ((self.class.order(order => :desc)
      .pluck(order).index(send(order)) + 1)
        .to_f / 3).ceil
  end

  # Build index elasticsearch
  def as_indexed_json(_options = {})
    as_json(
      only: [:id, :property_type, :code, :city, :zone,
             :reference, :operation_type, :price, :rooms,
             :bathrooms, :size, :description]
    )
  end

  def images_present?
    errors.add(:images, 'Debe agregar imágenes') if images.empty?
  end

  def unread_questions
    unread = questions.where(read: false).count
    unread > 0 ? "Preguntas (#{unread} sin leer)" : 'Preguntas'
  end

  def questions_class
    questions.where(read: false).any? ? 'keppler' : 'md-dark'
  end

  def rooms_text
    "#{rooms} Habitaciones"
  end

  def bathrooms_text
    "#{bathrooms} Baños"
  end

  def search_similar
    "#{zone} #{city} #{property_type}"
  end
end
