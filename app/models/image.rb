class Image < ActiveRecord::Base
  mount_uploader :img, PropertyImageUploader
  belongs_to :property

  validate :image_size_validation, if: 'img?'

  def image_size_validation
    if img.size > 1.megabytes
      errors.add(:img, 'Imagén no puede pesar más de 1 MB')
    end
  end

  def remove_image
    if !self.img.nil?
      image = File.dirname(Rails.root.join(self.img.to_s))
      FileUtils.rm_rf(image)
    end
  end
end
