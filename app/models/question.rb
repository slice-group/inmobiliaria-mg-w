# Question Model
class Question < ActiveRecord::Base
  include ElasticSearchable
  include ActivityHistory

  validates_presence_of :email, :message, :name
  # Phone validation
  validates :phone, length: { maximum: 13 }, allow_blank: true
  validates_numericality_of :phone, only_integer: true, allow_blank: true

  # Email validacion
  validates :email, format: {
    with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z/i
  }

  def self.query(query)
    { query: { multi_match: {
      query: query,
      fields: [:property_id, :name, :email, :phone, :message],
      operator: :and,
      lenient: true }
    }, sort: { id: 'desc' }, size: count }
  end

  # Build index elasticsearch
  def as_indexed_json(_options = {})
    as_json(
      only: [:id, :property_id, :name, :email, :phone, :message]
    )
  end

  def telephone
    phone.nil? || phone.empty? ? 'N/A' : phone
  end
end
