function removeHash () {
	history.pushState("", document.title, window.location.pathname + window.location.search);
}

function scrollToAnchor(anchor){
	$('#'+anchor).animatescroll({
		scrollSpeed:1500
	})
	removeHash ();
}

$(document).click(function(){
	$('.navbar-collapse').collapse('hide');
});