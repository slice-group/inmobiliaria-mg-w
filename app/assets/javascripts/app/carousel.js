$(document).ready(function(){
  initCarousel();

  $('.images.carousel').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    autoPlay: false,
    draggable: false,
    asNavFor: '.mini-images.carousel',
    dots: false,
    fade: true,
    arrows: false,
    accessibility: false,
    responsive: [
      {
        breakpoint: 767,
        settings: {
          arrows: false,
          dots: true
        }
      }
    ]
  });
  $('.mini-images.carousel').slick({
    slidesToShow: 4,
    slidesToScroll: 1,
    asNavFor: '.images.carousel',
    dots: false,
    centerMode: true,
    focusOnSelect: true,
    autoPlay: false,
    draggable: false,
    arrows: false,
    accessibility: false
  });

  var $window = $(window)
    , $properties = $('.properties.carousel')
    , toggleSlick;

  toggleSlick = function () {
    if ($window.width() < 450) {
      initCarousel();
    } else {
      if ($('.properties.carousel').hasClass('slick-initialized')) {
        $('.properties.carousel').slick('unslick');
      }
      if ($('.advisers').hasClass('slick-initialized')) {
        $('.advisers').slick('unslick');
      }
    }
  }

  $(window).resize(toggleSlick);
  toggleSlick();

});

function initCarousel() {
  initCustomers();
  $('.properties.carousel').slick({
    dots: false,
    infinite: true,
    speed: 300,
    slidesToShow: 3,
    slidesToScroll: 1,
    arrows: false,
    responsive: [
      {
        breakpoint: 991,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          arrows: false,
          dots: true
        }
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          arrows: false,
          dots: true
        }
      },
      {
        breakpoint: 360,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          arrows: false,
          dots: true
        }
      }
    ]
  });

  if ($(window).width() < 450) {
    $('.customers.centered-row').addClass('carousel').removeClass('centered-row');
    initCustomers();
    $('.advisers').slick({
      dots: true,
      infinite: true,
      speed: 300,
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: false
    });
  }
}

function initCustomers() {
  $('.customers.carousel').slick({
    dots: false,
    infinite: true,
    speed: 300,
    autoplay: true,
    slidesToShow: 4,
    slidesToScroll: 1,
    responsive: [
      {
        breakpoint: 991,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 1,
          arrows: true
        }
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
          arrows: true
        }
      },
      {
        breakpoint: 360,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          arrows: true
        }
      }
    ]
  });
}
