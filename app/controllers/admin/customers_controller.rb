module Admin
  # CustomersController
  class CustomersController < AdminController
    before_action :set_customer, only: [:show, :edit, :update, :destroy]
    before_action :show_history, only: [:index]

    # GET /customers
    def index
      customers = Customer.searching(@query).all
      @objects = customers.page(@current_page)
      @total = customers.size
      if !@objects.first_page? && @objects.size.zero?
        redirect_to customers_path(page: @current_page.to_i.pred, search: @query)
      end
    end

    # GET /customers/1
    def show
    end

    # GET /customers/new
    def new
      @customer = Customer.new
    end

    # GET /customers/1/edit
    def edit
    end

    # POST /customers
    def create
      @customer = Customer.new(customer_params)

      if @customer.save
        redirect(@customer, params)
      else
        render :new
      end
    end

    # PATCH/PUT /customers/1
    def update
      if @customer.update(customer_params)
        redirect(@customer, params)
      else
        render :edit
      end
    end

    # DELETE /customers/1
    def destroy
      @customer.destroy
      redirect_to admin_customers_path, notice: actions_messages(@customer)
    end

    def destroy_multiple
      Customer.destroy redefine_ids(params[:multiple_ids])
      redirect_to(
        admin_customers_path(page: @current_page, search: @query),
        notice: actions_messages(Customer.new)
      )
    end

    private

    # Use callbacks to share common setup or constraints between actions.
    def set_customer
      @customer = Customer.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def customer_params
      params.require(:customer).permit(:name, :image)
    end

    def show_history
      get_history(Customer)
    end
  end
end
