module Admin
  # PropertiesController
  class PropertiesController < AdminController
    before_action :set_property, only: [:show, :edit, :update, :destroy, :edit_room_images, :update_room_images,]
    before_action :show_history, only: [:index]

    # GET /properties
    def index
      properties = Property.searching(@query).all
      @objects = properties.page(@current_page)
      @total = properties.size
      if !@objects.first_page? && @objects.size.zero?
        redirect_to properties_path(page: @current_page.to_i.pred, search: @query)
      end
    end

    # GET /properties/1
    def show
      @property = @property.decorate
    end

    # GET /properties/new
    def new
      @property = Property.new
      @property_types = Property.property_types.transform_keys { |x| x.humanize }
      @operation_types = Property.operation_types.transform_keys { |x| x.humanize }
    end

    # GET /properties/1/edit
    def edit
    end

    # POST /properties
    def create
      @property = Property.new(property_params)

      if @property.save
        redirect(@property, params)
      else
        render :new
      end
    end

    # PATCH/PUT /properties/1
    def update
      if @property.update(property_params)
        redirect(@property, params)
      else
        if @property.errors.messages[:images]
          if @property.errors.messages[:images].first == 'is too long (maximum is 4 characters)'
            @property.errors.messages[:images] = ['Deben ser máximo 4 Imágenes']
          end
        end
        render :edit
      end
    end

    # DELETE /properties/1
    def destroy
      @property.destroy
      redirect_to admin_properties_path, notice: actions_messages(@property)
    end

    def destroy_multiple
      Property.destroy redefine_ids(params[:multiple_ids])
      redirect_to(
        admin_properties_path(page: @current_page, search: @query),
        notice: actions_messages(Property.new)
      )
    end

    private

    # Use callbacks to share common setup or constraints between actions.
    def set_property
      @property = Property.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def property_params
      params.require(:property)
            .permit(:property_type, :code, :city, :zone, :reference,
                    :operation_type, :price, :rooms, :bathrooms, :size,
                    :description, images_attributes: [:id, :img, :_destroy])
    end

    def show_history
      get_history(Property)
    end
  end
end
