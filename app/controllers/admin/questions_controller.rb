module Admin
  # QuestionsController
  class QuestionsController < AdminController
    before_action :set_question, only: [:show, :edit, :update, :destroy]
    before_action :set_property, only: [:index, :show, :create, :destroy]
    before_action :show_history, only: [:index]

    # GET /questions
    def index
      questions = Question.where(property_id: @property.id).searching(@query).all
      @objects = questions.page(@current_page)
      @total = questions.size
      if !@objects.first_page? && @objects.size.zero?
        redirect_to questions_path(page: @current_page.to_i.pred, search: @query)
      end
    end

    # GET /questions/1
    def show
      if params[:read].present?
        @question.update_attribute(:read, true) unless @question.read
      end
    end

    # GET /questions/new
    def new
      @question = Question.new
    end

    # GET /questions/1/edit
    def edit
    end

    # POST /questions
    def create
      @question = Question.new(question_params)
      @question.read = false
      @question.property_id = @property.id

      if @question.save
        redirect_to app_show_property_path @property.id
      else
        render :new
      end
    end

    # PATCH/PUT /questions/1
    def update
      if @question.update(question_params)
        redirect(@question, params)
      else
        render :edit
      end
    end

    # DELETE /questions/1
    def destroy
      @question.destroy
      redirect_to admin_property_questions_path, notice: actions_messages(@question)
    end

    def destroy_multiple
      Question.destroy redefine_ids(params[:multiple_ids])
      redirect_to(
        admin_property_questions_path(page: @current_page, search: @query),
        notice: actions_messages(Question.new)
      )
    end

    private

    # Use callbacks to share common setup or constraints between actions.
    def set_question
      @question = Question.find(params[:id])
    end

    def set_property
      @property = Property.find(params[:property_id])
    end

    # Only allow a trusted parameter "white list" through.
    def question_params
      params.require(:question).permit(:property_id, :name, :email, :phone, :message)
    end

    def show_history
      get_history(Question)
    end
  end
end
