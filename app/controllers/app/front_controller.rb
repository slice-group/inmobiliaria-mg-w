module App
  # FrontController
  class FrontController < AppController
    def index
      @properties = Property.all.sample 3
      @customers = Customer.all.shuffle
    end
  end
end
