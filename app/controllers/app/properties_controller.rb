module App
  # PropertiesController
  class PropertiesController < AppController
    before_filter :paginator_params, on: :index

    def index
      properties = Property.searching(@query).all
      @properties = properties.page(@current_page).per(6)
      @total = properties.size
      if !@properties.first_page? && @properties.size.zero?
        redirect_to properties_path(page: @current_page.to_i.pred, search: @query)
      end
    end

    def search
      properties = Property.searching(@query).all
      @properties = properties.page(@current_page).per(6)
      @total = properties.size
      if !@properties.first_page? && @properties.size.zero?
        redirect_to properties_path(page: @current_page.to_i.pred, search: @query)
      end
    end

    def show
      @property = Property.find_by code: params[:code]
      @question = Question.new

      @suggestions = Property.app_searching(@property.search_similar)
      @suggestions = limit_suggestions @suggestions
    end

    def paginator_params
      @query = params[:search] unless params[:search].blank?
      @current_page = params[:page] unless params[:page].blank?
    end

    def limit_suggestions(properties)
      properties.where.not(id: @property.id).limit 3
    end
  end
end
