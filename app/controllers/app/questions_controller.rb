module App
  # QuestionsController
  class QuestionsController < AppController
    before_action :set_property
    def create
      @question = Question.new(question_params)
      @question.read = false
      @question.property_id = @property.id

      if @question.save
        redirect_to app_property_path(@property.code), notice: 'Su pregunta ha sido enviada satisfactoriamente'
      else
        render template: 'app/properties/show'
      end
    end

    private

    def set_property
      @property = Property.find_by code: params[:property_code]
      @suggestions = Property.app_searching(@property.search_similar)
      @suggestions = limit_suggestions @suggestions
    end

    # Only allow a trusted parameter "white list" through.
    def question_params
      params.require(:question).permit(:property_id, :name, :email, :phone, :message)
    end

    def limit_suggestions(properties)
      properties.where.not(id: @property.id).limit 3
    end
  end
end
