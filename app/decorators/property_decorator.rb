class PropertyDecorator < Draper::Decorator
  #include Draper::LazyHelpers
  delegate_all

  def price
    h.number_to_currency( object.price, precision: 2, separator: ",", unit: 'Bs ' )
  end

  def size
    "#{object.size}mts2"
  end
end
