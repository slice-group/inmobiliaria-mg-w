class CreateQuestions < ActiveRecord::Migration
  def change
    create_table :questions do |t|
      t.string :name
      t.string :email
      t.string :phone
      t.text :message
      t.belongs_to :property

      t.timestamps null: false
    end
  end
end
