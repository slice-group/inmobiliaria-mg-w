class AddReadToQuestions < ActiveRecord::Migration
  def change
    add_column :questions, :read, :boolean
  end
end
