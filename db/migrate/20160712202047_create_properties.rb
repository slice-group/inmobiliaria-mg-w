class CreateProperties < ActiveRecord::Migration
  def change
    create_table :properties do |t|
      t.integer :type
      t.string :code
      t.string :city
      t.string :zone
      t.text :reference
      t.integer :operation_type
      t.float :price
      t.integer :rooms
      t.integer :bathrooms
      t.float :size
      t.text :description

      t.timestamps null: false
    end
  end
end
