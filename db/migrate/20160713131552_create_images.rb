class CreateImages < ActiveRecord::Migration
  def change
    create_table :images do |t|
      t.string :img
      t.belongs_to :property

      t.timestamps null: false
    end
  end
end
